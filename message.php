<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <title>Envoie du message</title>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="style/main.css" />
</head>

<body>
  <?php
  include('header.php');

  if( !isset($_POST['sujet'], $_POST['message']) ){
    header('location:index.php');
  }
  if( mail('desRestesUneRecette@gmail.com', $_POST['sujet'], $_POST['message']) ){
    echo'<p>Votre mail est en cours d\'envoie. Merci d\'avoir pris le temps de nous contacter.</p>';
  }
  else{
    echo '<p>Nous somme désolé mais votre mail n\'a pas pu être envoyé. Veuillez réessayez plus tard.</p>';
  }
  echo '<a href="index.php">Retour à la page d\'accueil.</a>';

  include('footer.php');
  ?>
</body>
</html>
