-- Adminer 4.8.0 MySQL 5.7.24 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `desserts`;
CREATE TABLE `desserts` (
  `id_recette` int(10) unsigned NOT NULL,
  `image` varchar(100) NOT NULL,
  `recette` varchar(100) NOT NULL,
  `ingredients` text NOT NULL,
  `difficulte` enum('1','2','3','4','5') NOT NULL,
  `temps` int(10) unsigned NOT NULL,
  KEY `id_recette` (`id_recette`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `desserts` (`id_recette`, `image`, `recette`, `ingredients`, `difficulte`, `temps`) VALUES
(47,	'../../images/recettes_toto/recette7.png',	'recettes/toto/recette7.php',	'beure;chocolat;farine;sucre',	'2',	45);

DROP TABLE IF EXISTS `entrees`;
CREATE TABLE `entrees` (
  `id_recette` int(10) unsigned NOT NULL,
  `image` varchar(100) NOT NULL,
  `recette` varchar(100) NOT NULL,
  `ingredients` text NOT NULL,
  `difficulte` enum('1','2','3','4','5') NOT NULL,
  `temps` int(10) unsigned NOT NULL,
  KEY `id_recette` (`id_recette`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `evaluations`;
CREATE TABLE `evaluations` (
  `id_utilisateur` int(10) unsigned NOT NULL,
  `id_recette` int(10) unsigned NOT NULL,
  `note` enum('0,1,2,3,4,5') NOT NULL,
  KEY `id_utilisateur` (`id_utilisateur`),
  KEY `id_recette` (`id_recette`),
  CONSTRAINT `evaluations_ibfk_1` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateurs` (`id`) ON DELETE CASCADE,
  CONSTRAINT `evaluations_ibfk_2` FOREIGN KEY (`id_recette`) REFERENCES `recettes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `plats`;
CREATE TABLE `plats` (
  `id_recette` int(10) unsigned NOT NULL,
  `image` varchar(100) NOT NULL,
  `recette` varchar(100) NOT NULL,
  `ingredients` text NOT NULL,
  `difficulte` enum('1','2','3','4','5') NOT NULL,
  `temps` int(10) unsigned NOT NULL,
  KEY `id_recette` (`id_recette`),
  CONSTRAINT `plats_ibfk_2` FOREIGN KEY (`id_recette`) REFERENCES `recettes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `plats` (`id_recette`, `image`, `recette`, `ingredients`, `difficulte`, `temps`) VALUES
(42,	'images/recettes_toto/recette1.png',	'recettes/toto/recette1.php',	'farine;lait;levure;poivrons;tomate',	'2',	35),
(43,	'../../images/recettes_toto/recette2.png',	'recettes/toto/recette2.php',	'haricots;levure;pates;posisson;riz',	'2',	65),
(44,	'../../images/recettes_toto/recette3.png',	'recettes/toto/recette3.php',	'farine;lait;riz;tomate',	'2',	45),
(45,	'../../images/recettes_toto/recette4.png',	'recettes/toto/recette4.php',	'haricots;huile;poivrons;riz',	'5',	85),
(46,	'../../images/recettes_toto/recette5.png',	'recettes/toto/recette5.php',	'farine;lait;poivrons;poulet',	'5',	85);

DROP TABLE IF EXISTS `recettes`;
CREATE TABLE `recettes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_chef` int(10) unsigned NOT NULL,
  `nom_recette` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_chef_nom_recette` (`id_chef`,`nom_recette`),
  CONSTRAINT `recettes_ibfk_1` FOREIGN KEY (`id_chef`) REFERENCES `utilisateurs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `recettes` (`id`, `id_chef`, `nom_recette`) VALUES
(21,	1,	'Lorem Ipsum'),
(42,	1,	'recette1'),
(43,	1,	'recette2'),
(44,	1,	'recette3'),
(45,	1,	'recette4'),
(46,	1,	'recette5'),
(47,	1,	'recette7');

DROP TABLE IF EXISTS `statut`;
CREATE TABLE `statut` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `statut` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `statut` (`id`, `statut`) VALUES
(0,	'standard'),
(1,	'chef'),
(2,	'root');

DROP TABLE IF EXISTS `totalevaluation`;
CREATE TABLE `totalevaluation` (
  `id_recette` int(10) unsigned NOT NULL,
  `note` float NOT NULL,
  `nombre` int(10) unsigned NOT NULL,
  KEY `id_recette` (`id_recette`),
  CONSTRAINT `totalevaluation_ibfk_1` FOREIGN KEY (`id_recette`) REFERENCES `recettes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `totalevaluation` (`id_recette`, `note`, `nombre`) VALUES
(42,	5,	1),
(43,	5,	1),
(44,	5,	1),
(45,	5,	1);

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE `utilisateurs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adresse_mail` varchar(100) NOT NULL,
  `pseudo` varchar(30) NOT NULL,
  `mdp` varchar(64) NOT NULL,
  `statut` int(10) unsigned NOT NULL,
  `photo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `statut` (`statut`),
  CONSTRAINT `utilisateurs_ibfk_1` FOREIGN KEY (`statut`) REFERENCES `statut` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `utilisateurs` (`id`, `adresse_mail`, `pseudo`, `mdp`, `statut`, `photo`) VALUES
(1,	'toto@to.to',	'toto',	'0abf65acceb8855cf727872a131430e8ce4e72d340d15f2be2bbb291feb989b6',	2,	'images/profils/defaut.png'),
(3,	'chef1@oo.fr',	'toto2',	'68f6d508f888ba3dc549c9e25e514fd22067b5b0162e2065b2b1c40970fcdcf2',	1,	'images/profils/defaut.png'),
(6,	'membre1@gmail.com',	'membre1',	'7d5d02719e2ecb4a9e7f7bb1989475a824079cdf5d0f3e5f3f48f58c46f6c92d',	0,	'images/profils/defaut.png'),
(7,	'membre2@gmail.com',	'membre2',	'c1228153aadb34fc51bc9ea7d107b19f25815e85613893084f6f54a80e4e4bdb',	0,	'images/profils/defaut.png'),
(8,	'membre3@gmail.com',	'membre3',	'52c6782252c3756e2b40725d758b4fb59540ef1775f638304e0d44cb55fc9632',	0,	'images/profils/defaut.png'),
(9,	'membre4@gmail.com',	'membre4',	'30dda08712308b331994f00af95813694365d96c52da8a43c144e7218cb8a027',	0,	'images/profils/defaut.png'),
(10,	'membre6@gmail.com',	'membre5',	'ea51c3c44303e89a7d1a7fa21f9efcd3c3f2f55477cccca3aafe11056182014a',	0,	'images/profils/defaut.png'),
(11,	'membre7@gmail.com',	'membre7',	'fe3c910de87fbf90dc97f166484a699e97026c89a07cb83b2c7cd78efeb40589',	0,	'images/profils/membre7.png'),
(12,	'membre8@gmail.com',	'membre8',	'f0022c66218e0ec6b161843b2c493a66c236cee200ac5fb519a655e673dcf4eb',	0,	'images/profils/membre8.png');

-- 2021-05-12 21:27:59
