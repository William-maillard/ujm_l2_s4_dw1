# UJM_L2_S4_DW1

Consignes : Réalisation d'un site web avec des membres connectés

technos : HTML5, CSS3, PhP, MySQL, Javascript

Projet : Site web permettant la recherche d'une recette en fonction des restes que l'on a dans le frigo.
Les membres connectés peuvent voter pour leurs recettes favorites.

Déploiement : https://desrestesunerecette.000webhostapp.com/
