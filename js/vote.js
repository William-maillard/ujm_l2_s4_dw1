//tableau associatif pour stocker les votes du l'utilisateur
var les_votes= new Array();

//fonction pour ajouter un vote dans un tableau associatif avec comme clé l'id de la recette
function ajouter_vote(cle, vote) {
  les_votes[cle]= vote;
  console.log('vote ajouté');
}

//fonction pour envoyer les votes du tableau les_votes via un formulaire
function ajouter_les_votes() {
  //on récupère l'input caché
  var input= document.querySelector('input[type="hidden"]');
  //on lui donne comme valeur le tableau qui contient les_votes
  input.values= les_votes;
  console.log('tableau envoyé');
}
