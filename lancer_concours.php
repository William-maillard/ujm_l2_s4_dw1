<?php
  session_start();
  include('connex.inc.php');

  if(!isset($_SESSION['statut'])  ||  $_SESSION['statut'] < 1) {
    /*--la personne n'est pas autorisée à consulter cette page, on la redirige--*/
    header('location:index.php');
  }

  /* ***** on a soumis un formulaire ***** */
  if(isset($_POST['type'])  &&  (isset($_POST['nom_recette'])  ||  isset($_POST['theme']))) {
    $pdo= connex($base);

    /*--on crée une table au nom du chef pour le concours--*/
    $table= $pdo->prepare('CREATE TABLE :nom(
                           id_membre INT,
                           image VARCHAR(100),
                           nb_like INT;
                           nb_dislike INT);');
  }

?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <title>Lancer-concours</title>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="style/main.css" />
</head>
<body>

  <?php include('header.php'); ?>

  <h1>Informations sur les concours</h1>

  <p>
    Bonjour <?php $_SESSION['pseudo']; ?>,  sur cette page vous pouvez lancer
    des concours celon les catégories suivantes :
  </p>

  <!-- liste des différentes catégories de concours disponibles -->
  <ol>
    <li>
      <span class="categorie_concours">Recette imposée</span> :
      choissisez une de vos recettes et challengez les membres du site pour
      la reproduire dans un temps donné.
    </li>
    <li>
      <span class="categorie_concours">Recette revisitée</span> :
      choissisez une de vos recettes et challengez les membres du site pour
      la revisiter dans un temps donné.
    </li>
    <li>
      <span class="categorie_concours">Recette créative</span> :
      choissisez un thème et challengez les membres du site pour créer une
      recette en rapport à ce thème.
    </li>
    <li>
      <span class="categorie_concours">Comming soon ...</span>
    </li>
  </ol>

  <!-- brève explication des concours -->
  <p>
    Lorsque vous démarrez un concours, les membres ont jusqu'à la date de fin
    pour poster la photo de leur recette , et voter.<br />
    Les chefs du site peuvent voter, chacun de leur vote rapporte 5 points au
    lieu de 1.<br />
    A la fin, les résultats seront visibles pendant un jour, puis nous conserveront
    juste les informations des 3 gagnants.
  </p>

  <h1>Lancer un concours</h1>

  <p>
    Vous trouverez ci-dessous un formulaire à compléter pour lancer un concours.
    Veuillez notez qu'il ne vous est possible de lancer qu'un concours à la fois.
    Donc si vous avez un concours en cours, veuillez attendre la fin du jour de
    résultat de ce dernier pour en relancer un.
  </p>

  <fieldset>
    <legend>Concours :</legend>
    <form action="" method="post">
      <select name="type">
        <option>Type concours</option>
        <option value="imposee">Recette imposée</option>
        <option value="revisitee">Recette revisitée</option>
        <option value="creative">Recette créative</option>
      </select>
      <br />
      <label id="nom">Nom recette :<input type="text" name="nom_recette" /></label>
      <br />
      <label id="theme">Thème :<input type="text" name="theme" /></label>
      <br />
      <button type="submit">Lancer</button>
    </form>
  </fieldset>

  <?php include('footer.php');  ?>

</body>
</html>
