<!-- pied de page de notre site qui propose :
     -un formulaire pour nous contacter
     -un lien pour revenir en haut de page
  -->
<footer>
    <h2>Nous contacter:</h2>

    <p>
      Pour nous envoyer des commentaires, postuler pour devenir contributeur de
      recettes...<br />

  <?php
  if(isset($_SESSION['pseudo'], $_SESSION['statut'])){
  ?>
  </p>
  <script src="js/compteur.js"></script>
  <form action="message.php" method="post">
    <fieldset>
      <legend>Nous contacter:</legend>
      <label>Sujet:
        <input type="text" name="sujet" maxlength="70" required />
      </label>
      <br />
      <textarea name="message" id="texte" maxlength="1000" placeholder="Votre message" oninput="textCompteur();">
      </textarea>
      <br />
      <input type="text" id="taille" readonly/>
      <br />
      <button>Envoyer</button>
    </fieldset>
  </form>

  <?php }else{ ?>
      Veuillez vous authentifier ou vous inscrire à l'aide des liens
      ci-dessous :<br />
    </p>
    <ul>
      <li>Vous avez déjà un compte : <a href="connexion.php">Se connecter</a></li>
      <li>Vous n'avez pas de compte : <a href="inscription.php">S'inscrire</a></li>
    </ul>
    <?php } ?>

    <p>
      Cliquez <a href="#header">ici</a> pour remonter en haut de page.
    </p>

</footer>
