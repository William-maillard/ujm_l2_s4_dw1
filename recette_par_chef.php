<?php
session_start();
include('connex.inc.php');

function afficher_chefs($statut,$pdo){
    $membres= $pdo->query("SELECT id, pseudo, adresse_mail, photo
                           FROM utilisateurs
                           WHERE statut=$statut;");

    //affichage dans un tableau
    foreach($membres as $membre)
    {
        echo '<table>
        <tr>
        <td><a href="recette_par_chef.php?id=' . $membre['id'] . '&pseudo=' . $membre['pseudo'] . '" ><img id="profil" src="' . $membre['photo'] . '" alt="image de profil" /></a>
        <br>
        ' . $membre['pseudo'] . '</td>
        <br>
        </tr>';
    }
    echo '</table>';
  }

function afficher_entrees($id, $pdo){
  $entrees= $pdo->query("SELECT *
                           FROM recettes
                           JOIN entrees ON id=id_recette
                           WHERE id_chef=$id;");

    foreach($entrees as $entree)
    {
        echo '<div id="resultat">';
        echo '<a class="recette" href="' . $entree["recette"] . '">';
        /*--affichage de l'image de la recette--*/
        echo '<img id="photo" src="' . $entree["image"] . '" alt="illustration de la recette" />';
        echo '</a>';
        echo '<br>';
        
        /*--affichage de la titre de la recette--*/
        echo 'Nom de l\'entrée : ';
        echo $entree['nom_recette'];
        echo '<br><br>';
        
        /*--fin du bloc d'informations--*/
        echo '</div>';
    }
  }

  function afficher_plats($id, $pdo){
  $plats= $pdo->query("SELECT *
                           FROM recettes
                           JOIN plats ON id=id_recette
                           WHERE id_chef=$id;");
  
  foreach($plats as $plat)
  {
  echo '<div id="resultat">';
    echo '<a class="recette" href="' . $plat["recette"] . '">';
      /*--affichage de l'image de la recette--*/
      echo '<img id="photo" src="' . $plat["image"] . '" alt="illustration de la recette" />';
      echo '</a>';
      echo '<br>';
      
      /*--affichage de la titre de la recette--*/
      echo 'Nom du plat : ';
      echo $plat['nom_recette'];
      echo '<br><br>';
      
      /*--fin du bloc d'informations--*/
      echo '</div>';
    }
 }
    
    
function afficher_desserts($id, $pdo){
    $desserts= $pdo->query("SELECT *
                           FROM recettes
                           JOIN desserts ON id=id_recette
                           WHERE id_chef=$id;");

    foreach($desserts as $dessert)
    {
        echo '<div id="resultat">';
        echo '<a class="recette" href="' . $dessert["recette"] . '">';
        /*--affichage de l'image de la recette--*/
        echo '<img id="photo" src="' . $dessert["image"] . '" alt="illustration de la recette" />';
        echo '</a>';
        echo '<br>';
        
        /*--affichage de la titre de la recette--*/
        echo 'Nom du dessert : ';
        echo $dessert['nom_recette'];
        echo '<br><br>';
        
        /*--fin du bloc d'informations--*/
        echo '</div>';
    }
}

?>

<!DOCTYPE html>
<html>
  <head>
    <title>Chefs</title>
    <link rel="stylesheet" href="style/main.css" />
  </head>

  <body>
    <?php
      include('header.php');
      $pdo= connex($base);
    ?>
    <h1>Voici les chefs du site :</h1>
    Pour voir toutes les recettes d'un chef, cliquez sur sa photo et les recettes s'afficheront.<br>
    Si rien ne s'affiche quand vous cliquez, cela signifie que ce chef a posté aucune recette.
    <?php
      afficher_chefs(1, $pdo);
      echo '<br><br>';
      if( isset($_GET['id']) ){
        $id = $_GET['id'];
      $pseudo = $_GET['pseudo'];
      echo '<h2>Toutes les recettes du chef '.$pseudo.':</h2>';
      echo '<h3>- les entrées:</h3>';
      afficher_entrees($id, $pdo);
      echo '<br><br>';
      echo '<h3>- les plats:</h3>';
      afficher_plats($id, $pdo);
      echo '<br><br>';
      echo '<h3>- les desserts:</h3>';
      afficher_desserts($id, $pdo);
      echo '<br>';
    }
    include('footer.php');
    ?>
  </body>
</html>