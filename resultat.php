<?php
  session_start();
  include('connex.inc.php');
  /*--si l'utilisateur n'a pas soumis de formulaire, on le redirige sur la page principale--*/
  if(!isset($_POST['ingredients'], $_POST['type']))
  {
    header('location:index.php?erreur=1');
  }
  else{
    /*--traitement des données reçues--*/
    switch($_POST['type']){
      case 0:
        $type='entrees';
        break;
      case 1:
        $type='plats';
        break;
      case 2:
        $type='desserts';
        break;
      default:
        header('location:index.php?erreur=2');
    }
    $tab_ingredients= $_POST['ingredients'];
    sort($tab_ingredients);
    $ingredients= implode('%', $tab_ingredients);
    $ingredients= $ingredients . '%';
    /*--requête dans la base de données--*/
    $pdo= connex($base);
    $select= $pdo->prepare('SELECT recettes.id AS id_recette, nom_recette, image,
                            recette, ingredients, difficulte, temps,
                            note, nombre
                            FROM recettes
                            JOIN ' . $type . ' ON recettes.id =' . $type .'.id_recette
                            JOIN totalevaluation ON recettes.id=totalevaluation.id_recette
                            WHERE ingredients LIKE :ingredients;');

    $select->bindParam(':ingredients', $ingredients);
    $select->execute();
    $ligne= $select->fetch(PDO::FETCH_ASSOC);

    if( !$ligne ){
      /*--pas de correspondance trouvé, on va réessayer avec moins d'ingrédients--*/
      header('location:index.php?trouve=0');
    }
  }

  /*--on a trouvé des recettes, on va les afficher--*/
 ?>
 <!DOCTYPE html>
 <html>
 <head>
   <title>Recettes trouvées</title>
   <meta charset="utf-8" />
   <link rel="stylesheet" href="style/main.css" />
   <link rel="stylesheet" href="style/resultat.css" />
   <script src="js/vote.js"></script>
 </head>
 <body>
   <?php include('header.php'); ?>

   <h2>Voici le résultat de votre recherche :</h2>
   <p>Voici la liste des ingrédients sélectionnés contenus dans les recettes proposées :<br />
     <?php
     $ingredients= str_replace('%', ' ', $ingredients);
     echo $ingredients ;
     ?>.
   </p>

     <div id="resultat">
       <?php
          /*--tant qu'il y a une recette, on affiche un lien vers la recette--*/
         while( $ligne ){
             echo '<div class="recette">';
             /*--affichage de l'image--*/
             echo '<img src="' . $ligne["image"] . '" alt="illustration de la recette" class="img_recette" />';

             /*--bloc d'information contenant sur la rectte + lien vers la recette--*/
             echo '<div class="infos">';
             echo '<a href="' . $ligne["recette"] . '" target="_blank">'. $ligne['nom_recette'] . '</a><br />';

             /*--affichage de la note de la recette--*/
             echo '<span class="legend">note:<span><br />';
             $note= $ligne['note'] / $ligne['nombre'];

             for($i=0; $i<5; $i++){
                 if($note > 0){
                     echo '<img src="images/icones/etoile_pleinne.png" alt="étoile pleinne" />';
                 }
                 else{
                     echo '<img src="images/icones/etoile_vide.png" alt="étoile vide" />';
                 }
              $note--;
            }
            echo '<br />';

            /*--affichage de la difficutée de la recette--*/
            echo '<span class="legend">difficulté :<span><br />';
            for($i=0; $i<$ligne['difficulte']; $i++){
              echo '<img src="images/icones/toque.png" alt="+1" />';
            }
            echo '<br />';

            /*--affichage du temps de préparation sous forme de petites pendules--*/
            echo '<span class="legende">temps :<br /><span>';
            $temps= $ligne['temps'];
            while($temps > 60){
              echo '<img src="images/icones/temps/60min.png" alt="60min" />';
              $temps/= 60;
            }
            if($temps >= 10){
              echo '<img src="images/icones/temps/' . $temps . 'min.png" alt="'. $temps . 'min" />';
            }

            /*--fin du bloc d'informations--*/
            echo '</div>';

            /*--bouton de vote--*/
            echo '<div class="vote">
                    <img src="images/icones/pouce-haut.png" alt="+1" class="plus" onclick="ajouter_vote('. $ligne["id_recette"] .', 1);" />
                    <img src="images/icones/pouce-bas.png" alt="-1" class="moins" onclick="ajouter_vote('. $ligne["id_recette"] .', -1);"/>
                  </div>';

            /*--fin de la carte--*/
            echo '</div>';

            /*--ligne suivante--*/
            $ligne= $select->fetch(PDO::FETCH_ASSOC);
          }
          $select->closeCursor();
          $pdo= null;
        ?>
      <!-- fin du conteneur des résultats -->
     </div>

     <!-- formulaire pour valider les votes -->
     <form action="" method="post">
         <input type="hidden" name="votes" />
         <button type="submit" onclick="ajouter_les_votes();">
           Confirmer les votes<br >
           et quitter
         </button>
     </form>

     <?php include('footer.php'); ?>

 </body>
</html>
