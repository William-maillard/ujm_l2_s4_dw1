<!-- haut de page de notre site qui propose :
     -logo du site
     -des boutons connexion et inscription
     -le menu de navigation
  -->

<?php
  function afficher_membre($pseudo, $photo){
      echo '<img id="profil" src="/'.$photo.'" alt="image du profil"/>';
      echo '<p>';
      echo $_SESSION['pseudo'];
      echo '</p>';
      echo '<a id="deco" class="bouton" href="deconnexion.php">Déconnexion</a>';
  }
?>
<header id="header">
  <img id="logo" src="/images/logo.png" alt="image du logo"/>
  <?php
   if( isset($_SESSION['pseudo']) && isset($_SESSION['photo']) ){
     afficher_membre($_SESSION['pseudo'],$_SESSION['photo']);
  ?>
  <?php } else{ ?>
  <a id="co" class="bouton" href="connexion.php">Se connecter</a>
  <a id="inscri" class="bouton" href="inscription.php">S'inscrire</a>
  <?php } ?>
</header>

<nav class="menu">
  <ul>
    <?php
      if(isset($_SESSION['statut'])){
        echo '<li><a href="/ajouter_recette.php">Ajouter une recette</a></li>';
      }else{
        echo '<li><a href="/recette_par_chef.php">recettes par chefs</a></li>';
      }
    ?>
    <li><a href="/index.php">Rechercher une recette</a></li>
    <li><a href="/membres.php">Membres</a></li>
    <li><a href="">Concours (Comming soon...)</a></li>
  </ul>
</nav>
