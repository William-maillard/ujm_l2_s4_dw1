<?php
  session_start();
  include('connex.inc.php');

   /* ***** on test si l'utilisateur est déjà connecté ***** */
      if( isset($_SESSION['pseudo'], $_SESSION['statut']) )
      {
          //si oui on le redirige vers la page principale
          header("location:index.php");
      }
      elseif( isset($_COOKIE['mail'], $_COOKIE['mdp']) ){
        /*--si l'utilisateur a des cookies pour redémarrer sa session--*/
        $mail= $_COOKIE['mail'];
        $mdp= $_COOKIE['mdp'];
      }
      else{
          $affiche_formulaire= 1;
          /* ***** on test si l'utilisaterur à soumis un formulaire ***** */
          if( isset($_POST['mail'], $_POST['mdp'])  ||  isset($mail, $mdp) ){
              $pdo= connex($base);
              $affiche_formulaire= 0;

              if(!isset($mail, $mdp)){
                // on traite les données envoyées
                $mail= filter_input(INPUT_POST, 'mail', FILTER_SANITIZE_EMAIL);
                $mdp= hash("sha256", $_POST['mdp'] . "drst1re7");
              }

              //on prepare une requête et lui affecte les variables
              $select= $pdo->prepare('SELECT * FROM utilisateurs WHERE adresse_mail= :mail');
              $select->bindParam(':mail', $mail);

              //on regarde si le mail correspond à un utilisateur
              $select->execute();
              if( ($utilisateur= $select->fetch()) )
              {
                  if($utilisateur['mdp'] === $mdp)
                  {
                      //on défini les variables de session
                      $_SESSION['pseudo']= $utilisateur['pseudo'];
                      $_SESSION['id']= $utilisateur['id'];
                      $_SESSION['adresse_mail']= $utilisateur['adresse_mail'];
                      $_SESSION['statut']= $utilisateur['statut'];
                      $_SESSION['photo']= $utilisateur['photo'];

                      //si l'utilisateur à cocher rester connecté, on créer des cookies en mode httpOnly
                      if(isset($_POST['rester_connecte']))
                      {
                          setcookie('mail', $_SESSION['adresse_mail'], time() + 30 * 24 * 3600, null, null, false, true);
                          setcookie('mdp', $_SESSION['mdp'], time() + 30 * 24 * 3600, null, null, false, true);

                      }
                      //on redirige l'utilisateur sur la page principale
                      header('location:index.php');
                  }
                  else{
                      echo '<p>Le mot de passe est invalide. Veuillez réessayer.</p>';
                      $affiche_formulaire= 1;
                  }
              }
              else{
                  echo 'Erreur l\'adresse mail ' . $mail . ' ne correspond à aucun compte existant.<br />';
                  echo 'Vérifier que vous avez bien saisie votre adresse mail, où inscriver vous <a href="inscription.php">ici</a></p>';
                  $affiche_formulaire= 1;
              }
          }
          $pdo= null;
      }

  /* ***** On affiche le formulaire si l'utilisateur n'est pas connecté ou que la connexion à échouée ***** */
  if($affiche_formulaire > 0)
  {
?>

<!DOCTYPE html>
<html lang="fr">
  <head>

    <title>Connexion</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="style/main.css" />
  </head>

  <body>

    <?php
      include('header.php');
      ?>

    <fieldset>
      <legend>Connexion :</legend>
      <form action="" method="post">

        <label>Adresse mail :<input type="email" name="mail" required /></label>
        <br />
        <label>mot-de-passe :<input type="password" name="mdp" required /></label>
        <br />
        <label class="radio"><input type="checkbox" name="rester_connecte" />Rester connecté</label>
        <br />
        <button type ="submit">Connexion</button>

      </form>
    </fieldset>

<?php include('footer.php'); ?>


  </body>
</html>
<?php } ?>
