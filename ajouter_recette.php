<?php
  session_start();
  include('connex.inc.php');

  if($_SESSION['statut'] < 1)
  {
      header('location:index.php');

  }
  ?>

  <!DOCTYPE html>
  <html lang="fr">
    <head>
      <title>Ajouter recette</title>
      <meta charset="utf-8" />
      <link rel="stylesheet" href="style/main.css" />
    </head>
    <body>

<?php include("header.php");
  $affiche=1;

  /* ***** un formulaire de recette à été envoyé ***** */
  if( isset($_POST['nom'], $_POST['temps'], $_POST['difficulte'], $_FILES['image'], $_POST['ingredients'], $_POST['titres_parties'], $_POST['parties']) )
  {
    $affiche=0;
    $pdo= connex($base);

    //on regarde si une recette du même nom du chef connecté n'existe pas déjà
    $nom= filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_STRING);
    $select= $pdo->prepare('SELECT COUNT(*) as recette
                            FROM recettes
                            JOIN utilisateurs ON id_chef=utilisateurs.id
                            WHERE nom_recette=:nom
                            AND  utilisateurs.pseudo=:pseudo;');
    $select->bindParam(':nom', $nom);
    $select->bindParam(':pseudo', $_SESSION['pseudo']);
    $select->execute();
    $exist= $select->fetch();
    if( $exist['recette'] == 1){
      echo '<p>Vous avez déjà créer une recette du même nom. Cliquez <a href="recettes/' . $_SESSION['pseudo']. '/' . $nom . '.php">ici</a> pour la consulter.</p>';
      $affiche= 1;
    }
    else{
      /* ***** traitement des variables (filter input ne marche pas sur les tableau, retourne un bool au lieu du tableau)***** */
      $temps= intval($_POST['temps']);
      $difficulte= intval($_POST['difficulte']);
      $tab_ingredients= $_POST['ingredients'];
      $titres= $_POST['titres_parties'];
      $parties= $_POST['parties'];
      $parties= str_replace("\n", '<br />', $parties);
      $conseils= $_POST['conseils'];

      //on trie le tableau d'ingrédients et le transforme en chaine que l'on stockera dans la base
      if( !sort($tab_ingredients, SORT_STRING) ){
        echo 'Une erreur inatendu est survenu, échec de l\'ajout de la recette.';
        $affiche= 1;
      }
      else{
        $ingredients= implode(';', $tab_ingredients);
      }

      //on change le chemin de l'image
      $chemin_image='images/recettes_' . $_SESSION['pseudo'] . '/' . $nom . '.png';
      move_uploaded_file($_FILES['image']['tmp_name'], $chemin_image);

      /* ***** on crée un fichier php contenant la recette ***** */
      $chemin_fichier='recettes/' . $_SESSION['pseudo'] . '/' . $nom . '.php';
      if( ($file= fopen($chemin_fichier, 'w')) )
      {
        //on créer le début du fichier
        $lignes="<?php session_start();  ?>\n
                <!DOCTYPE html>\n
                <html lang=\"fr\">
                <head>\n
                <title>Recette</title>\n
                <meta charset=\"utf-8\" />\n
                <link rel=\"stylesheet\" href=\"../../style/recette.css\">\n
                <link rel=\"stylesheet\" href=\"../../style/main.css\">\n
                </head>\n
                <body>\n
                <?php include('../../header.php');  ?>\n
                <section>\n
                <h1>" . $nom . "</h1>\n";
        fwrite($file, $lignes);

        //on insère l'image et la liste des ingrédients
        $chemin_image='../../' . $chemin_image;

        fprintf($file, '<img src="%s" alt="illustration de la recette" />', $chemin_image);
        fwrite($file, "\n<ul>\n");
        foreach($tab_ingredients as $ingredient){
          fprintf($file, "<li>%s</li>\n", $ingredient);
        }
        fwrite($file, "</ul>\n");

        //on insère les différentes parties
        $n= count($parties);
        for($i=0; $i<$n; $i++)
        {
          //titre partie
          fprintf($file, "<h2>%s</h2>\n", $titres[$i]);
          //conseils de préparation
          fwrite($file, '<div class="conseils">');
          $m= $i+2;
          for($j=$i; $j<=$m; $j++)
          {
            if($conseils[$j] != "")
            {
              fprintf($file, "\n<p>%s</p>", $conseils[$j]);
            }
          }
          fwrite($file, "</div>\n");
          //paragrphe de la partie.
          fprintf($file, "<p>%s</p>\n", $parties[$i]);
        }

        //on insère la fin de la page
        $lignes="</section>\n
                 <?php include('../../footer.php'); ?>\n
                 </body>\n
                 </html>";
        fwrite($file, $lignes);
        fclose($file);

          /* ***** on insère la recette dans la base de données ***** */
          //on insère la recette
          $insertion= $pdo->prepare('INSERT INTO recettes (id_chef, nom_recette)
                                     VALUES (:id_chef, :nom);');

          $insertion->bindParam(':id_chef', $_SESSION['id']);
          $insertion->bindParam(':nom', $nom);

          $insertion->execute();

          //on récupère l'id de la recette crée
          $select= $pdo->prepare('SELECT id FROM recettes
                                  WHERE nom_recette=:nom
                                  AND id_chef=:id;');

          $select->bindParam(':nom', $nom);
          $select->bindParam(':id', $_SESSION['id']);

          $select->execute();
          $recette= $select->fetch();
          $id= $recette['id'];
          $select->closeCursor();

          //on insère les infos de la recettes
          $insertion= $pdo->prepare('INSERT INTO ' . $_POST['type'] . ' (id_recette, recette, ingredients, image, temps, difficulte)
                                     VALUES (:id, :recette, :ingredients, :image, :temps, :difficulte)');

          $insertion->bindParam(':recette', $chemin_fichier);
          $insertion->bindParam(':ingredients', $ingredients);
          $insertion->bindParam(':image', $chemin_image);
          $insertion->bindParam(':temps', $temps);
          $insertion->bindParam(':difficulte', $difficulte);
          $insertion->bindParam(':id', $id);

          if( $insertion->execute() )
          {
            //l'insertion a bien eu lieux, on redirige sur la page crée
            header("location:$chemin_fichier");
          }
          else{
            unset($_GET['nb_ingredients'], $_GET['nb_parties']);
            echo '<p>Erreur, la recette n\'a pu être inséré dans la base de données.<p>';
            $affiche= 1;
          }

      $pdo= null;
    }
    else{
      echo 'Erreur de création du fichier contenant la recette. Veuillez réessayé ulterieurement.';
      $affiche= 1;
    }
  }
}

if($affiche == 1)
{

         /* ***** un formulaire d'informations sur la recette à été envoyé ***** */
        if( isset($_GET['nb_ingredients'], $_GET['nb_parties']) )
        {
            //on vérifie les données envoyées
            $nb_ingredients= $_GET['nb_ingredients'];
            $nb_parties= $_GET['nb_parties'];
            if($nb_ingredients < 4)
            {
              $nb_ingredients= 4;
            }
            elseif($nb_ingredients > 20)
            {
              $nb_ingredients= 20;
            }
            if($nb_parties < 1)
            {
              $nb_parties= 1;
            }
            elseif($nb_parties > 4)
            {
              $nb_parties= 4;
            }
            ?>

    <!-- On affiche le formulaire -->
    <h1>Ajouter une recette</h1>
    <p>Il ne vous reste plus qu'à compléter ce formulaire pour ajouter votre recette.</p>

    <form method="post" action="" enctype="multipart/form-data">
      <fieldset>
        <legend>RECETTE</legend>

        <!-- informations principales sur  la recette -->
        <label>nom : <input type="text" name="nom" maxlength="50" required/></label>
        <br />
          <label  class="radio"><input type="radio" name="type" value="entrees" />Entrée</label>
          <label class="radio"><input type="radio" name="type" value="plats" checked/>Plat</label>
          <label class="radio"><input type="radio" name="type" value="desserts" />Dessert</label>
        </label>
        <br />
        <label>temps de préparation (approximatif, en minutes) :
          <input type="number" name="temps" min="10" max="180" step="5" placeholder="multiple de 5" required/>
        </label>
        <br />
        <label>difficulté (1:débutant, 5:avancé) :
          <input type="number" name="difficulte" min="1" max="5" required />
        </label>
        <br />
        <label>Image du plat : <input type="file" name="image" accept="image/png"required/></label>
        <br />
        <label>Liste des ingrédients (avec leur quantité à droite) :
          <ul>
            <?php
            for($i=1; $i<=$nb_ingredients; $i++)
            {
              echo '<li><input type="text" name="ingredients[]" required/></li>';
            }
             ?>
          </ul>
        </label>

        <!-- affichage des parties -->
        <?php
        for($i=1; $i<=$nb_parties; $i++)
        {
        echo '<fieldset>
                <legend>Partie ' . $i .'</legend>';
        echo '<label>Titre :
                <input type="text" name="titres_parties[]" maxlength="50" required/>
                </label>
                <br />
                <br />';
          echo '<label>Les étapes de la partie :
                <br />
                <textarea name="parties[]" required>
                </textarea>
                </label>
                <fieldset>
                <legend>Conseils de préparation (optionnels)</legend>
                <ul>';

            //on peut donner jusqu'à trois conseils de préparation par partie
            for($j=0; $j<3; $j++)
            {
              echo'<li>
                      <input type="text" name="conseils[]" />
                    </li>';
            }
            echo '</ul>
                </fieldset>
            </fieldset>';
          }
            ?>
            <button type="submit">Ajouter</button>
          </fieldset>
      </form>

    <?php
    /* ***** sinon on affiche le formulaire sur la structure de la recette ***** */
    }else{
            ?>

            <h1>Informations générales sur votre recette</h1>
            <p>Bonjour <?php echo $_SESSION['pseudo']; ?>, pour ajouter une recette veuillez commencer par nous informer de sa structure à l'aide du formulaire ci-dessous.
            <form action="" method="get">
              <fieldset>
                <legend>Structure de votre recette :</legend>
                <label>Nombre d'ingrédients :
                  <input type="number" name="nb_ingredients" min="4" max="20" value="4" />
                </label>
                <br />
                <label>Nombre de parties :
                  <input type="number" name="nb_parties" min="1" max="4" value="1" />
                </label>
                <br />
                <button type="submit">Valider</button>
              </fieldset>
            </form>

      <?php
      }
       include('footer.php');
       echo "</body>\n</html>";
     }
?>
