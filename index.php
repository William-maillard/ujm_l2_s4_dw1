<?php
  session_start();
 ?>
<!DOCTYPE html>
<html lang="fr">
  <head>

    <title>index</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="style/main.css" />

  </head>

  <body>

    <?php
      include('header.php');

      if( isset($_POST['votes']) ) {
        include('connexion.inc.php');
        $pdo= connex($base);
        /*--il faut insérer les votes dans la BDD--*/
        foreach($_POST['votes'] as $id => $vote) {
          $pdo->exec('UPDATE totalevaluation
                      SET note= note + ' . $vote * 5 . ', nombre= nombre + 1
                      WHERE id_recette = ' . $id . ';');
        }
        $pdo= null;
      }

      if(isset($_GET['trouve'])){
        echo 'Aucune recette contenant vos ingrédients n\'a été trouvée.<br />
              Réessayer avec plus ou moins d\'ingrédients et/ou des ingrédients différents si possible.';
      }
      ?>
    <p>Choississez entre 3 à 5 ingrédients pour trouver une recette</p>

  <fieldset>
    <legend>Quels sont les ingrédients dont vous disposez ?</legend>
    <form action="resultat.php" method="post">
      <?php
      for($i= 1; $i < 6; $i++){
        echo '<label>Ingrédient '.$i.': </label><input type="text" name="ingredients[]" ';
        if($i<4){
          echo 'required ';
        }
        echo '/><br />';
      }
      ?>
      <label class="radio"><input type="radio" name="type" value="0" required/>Entrée</label>
      <label class="radio"><input type="radio" name="type" value="1" required/>Plat</label>
      <label class="radio"><input type="radio" name="type" value="2" required/>Dessert</label>
      <br />
      <button type ="submit">Valider</button>
    </form>
  </fieldset>
    <?php
          include('footer.php');
    ?>

  </body>
</html>
