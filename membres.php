<?php
  session_start();
  include('connex.inc.php');

  function afficher_membre_par_statut($statut, $pdo){

    $membres= $pdo->query("SELECT id, pseudo, adresse_mail, photo
                         FROM utilisateurs
                         WHERE statut=$statut;");

    //affichage dans un tableau
    echo '<table>
            <tr>
              <th>Pseudo</th>
              <th>Profil</th>';
    if($_SESSION['statut'] == 2)
    {
      echo '<th>adresse mail</th>';
      //on ne peut pas modifier les utilisateurs root
      if($statut != 2)
      {
            echo '<th>Modifier</th>';
      }
    }
    echo '</tr>';

    foreach($membres as $membre)
    {
      echo '<tr>
              <td>' . $membre['pseudo'] . '</td>
              <td><img src="' . $membre['photo'] . '" alt="image de profil" /></td>';

      if($_SESSION['statut'] == 2)
      {
        echo '<td>' . $membre['adresse_mail'] . '</td>';
        if($statut != 2)
        {
              echo '<td><a href="membres.php?id=' . $membre['id'] . '">X</a></td>';
        }
      }

      echo '</tr>';
    }

    echo '</table>';
  }

function formulaire_modifier_membre($membre){
  if($membre['statut'] == 0){
    $statut='standard';
  }
  else{
    $statut='chef';
  }
  echo '<fieldset>
          <legend>Modifier le membre</legend>
          <form action="" method="post">
            <label>pseudo :
            <input type="text" name="pseudo" placeholder="' . $membre['pseudo'] . '" />
            </label>
            <br />
            <select name="statut">
              <option value="">staut actuel : ' . $statut . '</option>
              <option value="0">standard</option>
              <option value="1">chef</option>
              <option value="2">root</option>
            </select>
            <br />
            Supprimer l\'image de profil :
            <label><input type="radio" name="photo" value="0" checked/>Non</label>
            <label><input type="radio" name="photo" value="1" />oui</label>
            <br />
            <input type="hidden" name="id" value="' . $_GET['id'] . '" />
            <button type="submit">Confirmer</button>
          </form>
        </fieldset>';
}
 ?>
<!DOCTYPE html>
<html>

<head>
  <title>Membres</title>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="style/main.css" />
  <link rel="stylesheet" href="style/membres.css" />
</head>

<body>
  <?php
    include('header.php');
    $pdo= connex($base);

    /* ***** Formulaire de modification de membres envoyé ***** */
    if(isset($_POST['pseudo'])  ||  isset($_POST['statut'])  ||  isset($_POST['photo']) )
    {
      //variables pour compter le nbr de modifications et d'erreurs
      $modif=0;
      $erreur=0;
      /*--mise à jour du pseudo--*/
      if( isset($_POST['pseudo'])  &&  $_POST['pseudo'] !== "" )
      {
        $update= $pdo->prepare('UPDATE utilisateurs
                                SET pseudo= :pseudo
                                WHERE id= :id;');

        $update->bindParam(':pseudo', $_POST['pseudo']);
        $update->bindParam(':id', intval($_POST['id']) );

        if( ! $update->execute() )
          {
            $erreur++;
          }
          else{
            $modif++;
          }
      }

      /*--mise à jour du statut :
            -si le membre devient chef on lui crée ses répertoires de recettes et images
            -si c'est l'inverse on ne supprime pas les recettes du chef
           --*/
      if( isset($_POST['statut'])  &&  $_POST['statut'] !== "" )
      {
        $select= $pdo->prepare('SELECT pseudo FROM utilisateurs
                                WHERE id= :id;');

        $update= $pdo->prepare('UPDATE utilisateurs
                                SET statut= :statut
                                WHERE id= :id ;');

        $select->bindParam(':id', intval($_POST['id']) );
        $update->bindParam(':statut', $_POST['statut'] );
        $update->bindParam(':id', intval($_POST['id']) );

        if( ! $update->execute() )
        {
          $erreur++;
        }
        elseif( $select->execute() )
        {
          if($_POST['statut'] == 1)
          {
            $cursor= $select->fetch();
            $rep_recette= 'recettes/' . $cursor['pseudo'] ;
            $rep_image= 'images/recette_' . $cursor['pseudo'] ;

            //si les répertoires n'existent pas, on les crée
            if( !is_dir($rep_image) )
            {
              mkdir($rep_image);
            }
            if( !is_dir($rep_recette) )
            {
              mkdir($rep_recette);
            }
          }
          $modif++;
        }
      }

      /*--supression de l'image du membre--*/
      if( isset($_POST['photo'])  &&  $_POST['photo'] == 1)
      {
        //supression de l'image
        $select= $pdo->prepare('SELECT photo FROM utilisateurs
                                WHERE id= :id;');

        $select->bindParam(':id', $_POST['id']);
        $select->execute();
        $chemin= $select->fetch();
        if( $chemin ===  "images/profils/defaut.png" )
        {
          $erreur++;
        }
        else{
          /*--suppression du fichier .png--*/
          unlink($chemin['photo']);
          $update= $pdo->prepare('UPDATE utilisateurs
                                  SET photo="images/profils/defaut.png"
                                  WHERE id= :id;');

          $update->bindParam(':id', intval($_POST['id']) );
          if($update->execute())
          {
            $modif++;
          }
          else{
            $erreur++;
          }
        }
      }

        //redirige sur la même page avec le nombre de modif et d'erreur en GET
        header("location:membres.php?modif=$modif&erreur=$erreur");
    }

      /* ***** Demande de modification d'un membre ***** */
    elseif( isset($_GET['id']) )
      {
        $id= intval($_GET['id']);

        //on vérifie que l'id correspond à un membre existant
        $membre= $pdo->query('SELECT * FROM utilisateurs WHERE id=' . $id. ';');
        $membre= $membre->fetch();

        if($membre)
        {
          if($membre['statut'] != 2)
          {
            formulaire_modifier_membre($membre);
            unset($_GET['id']);
          }
        }
      }
    else{
      /* ***** si il y a eu des modifications on affiche un message ***** */
      if( isset($_GET['modif'], $_GET['erreur']))
      {
        echo '<p>';
        //pas d'erreurs
        if($_GET['erreur'] == 0)
        {
          if($_GET['modif'] == 1)
          {
            echo 'La modification a été réalisée avec succès.';
          }
          else{
            echo 'Les modifications ont étés réalisés avec succès.';
          }
        }
        //des erreurs
        else{
          if($_GET['erreur'] == 1)
          {
            echo 'Une erreur est survenue, ';
          }
          else{
            echo 'Des erreurs sont survenues, ';
          }
          echo 'échec de la mise à jour des informations.</p>';
        }
        unset($_GET['modif'], $_GET['erreur']);
      }
      /* ***** affichage des membres du site ***** */
      ?>
  <h1>Voici les chefs du site :</h1>
    <?php
      afficher_membre_par_statut(1, $pdo);

      if($_SESSION['statut'] == 2)
      {
        echo '<h1>Voici les membres du site :</h1>';
        afficher_membre_par_statut(0, $pdo);
        echo '<h1>Voici les membres root du site:</h1>';
        afficher_membre_par_statut(2, $pdo);
      }
    }
      $pdo= null;
      include('footer.php');
    ?>
</body>

</html>
