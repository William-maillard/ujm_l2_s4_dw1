<?php
session_start();
include('connex.inc.php');
if( isset($_SESSION['pseudo'], $_SESSION['statut']) ){
    $_SESSION = array();
    session_destroy();
    header('location:index.php');
}
?>