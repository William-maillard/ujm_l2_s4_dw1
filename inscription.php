<?php
  session_start();
  include('connex.inc.php');
?>

<!DOCTYPE html>
<html lang="fr">
  <head>

    <title>Inscription</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="style/main.css" />

  </head>

  <body>

    <?php
      include('header.php');


      /* ***** on test si l'utilisateur est déjà connecté ***** */
      if( isset($_SESSION['pseudo'], $_SESSION['statut']) )
      {
          echo '<p>Vous êtes déjà connecté. Retour à la page d\'acceuil : <a href="index.php">ici</a>';
          $affiche_formulaire= 0;
      }
      else{
          $affiche_formulaire= 1;
          /* ***** on test si l'utilisaterur à soumis un formulaire ***** */
          if(isset($_POST['mail'], $_POST['pseudo'], $_POST['mdp'], $_POST['mdp_verif']) )
          {
              $pdo= connex($base);
              $affiche_formulaire= 0;

              // on traite les données envoyées
              $mail= filter_input(INPUT_POST, 'mail', FILTER_SANITIZE_EMAIL);
              $pseudo= filter_input(INPUT_POST, 'pseudo', FILTER_SANITIZE_STRING);
              $mdp= $_POST['mdp'];

              // on test si l'utilisateur à rentrer les 2 mêmes mot de passe
              if($mdp != $_POST['mdp_verif'])
              {
                  echo '<p> Les mots de passe ne correspondent pas.</p>';
                  $affiche_formulaire++;
              }
              else{
                  //on crypte le mdp
                  $mdp= hash("sha256", $mdp . "drst1re7");

                  //préparation des requêtes
                  $select_mail= $pdo->prepare('SELECT COUNT(*) as count FROM utilisateurs WHERE adresse_mail = :valeur');
                  $select_pseudo= $pdo->prepare('SELECT COUNT(*) as count FROM utilisateurs WHERE pseudo = :valeur');

                  //on exécute les requêtes et récupère le tableau associatif résultant
                  $select_mail->bindParam(':valeur', $mail);
                  $select_pseudo->bindParam(':valeur', $pseudo);
                  $select_mail->execute();
                  $select_mail= $select_mail->fetch();
                  $select_pseudo->execute();
                  $select_pseudo= $select_pseudo->fetch();

                  if($select_mail['count'] != 0)
                  {
                    echo '<p>L\'adresse mail correspond à un compte existant.</p>';
                    $affiche_formulaire++;
                  }
                  elseif($select_pseudo['count'] != 0)
                  {
                    echo '<p>Le pseudo ' . $pseudo . ' est déjà pris.</p>';
                     $affiche_formulaire++;
                  }
                  //le mail et pseudo ne sont pas utilisés
                  else{
                    /* ***** on ajoute l'utilisateur à la base de données ***** */

                    // si on a une image on lui attribut un chemin, sinon on donne le chemin de l'image par défaut
                    $chemin='images/profils/' . $pseudo . '.png';
                    if( !isset($_FILES['image'])  ||  !move_uploaded_file($_FILES['image']['tmp_name'], $chemin) )
                    {
                        $chemin= 'images/profils/defaut.png';
                    }

                    //on prepare la requête et on lui affecte les valeurs des variables
                    $insertion= $pdo->prepare('INSERT INTO utilisateurs (adresse_mail, pseudo, mdp, statut, photo) VALUES(:mail, :pseudo , :mdp, 0, :chemin);');
                    $insertion->bindParam(':mail', $mail);
                    $insertion->bindParam(':pseudo', $pseudo);
                    $insertion->bindParam(':mdp', $mdp);
                    $insertion->bindParam(':chemin', $chemin);


                    //on execute et test l'insertion
                    if($insertion->execute())
                    {
                      echo '<p>Inscription réalisée avec succès.<br />';
                      echo '<a href="connexion.php">Se connecter</a> ?</p>';
                    }
                    else{
                       echo '<p>Erreur d\'inscription, veuillez réessayer.</p>';
                       $affiche_formulaire++;
                   }
                 }
              }
            }
          $pdo= null;
      }

      if($affiche_formulaire > 0)
      {
    ?>

    <fieldset>
      <legend>Inscription :</legend>
      <form action="" method="post" enctype="multipart/form-data">

        <label>Adresse mail :<input type="email" name="mail" required /></label>
        <br />
        <label>pseudo :<input type="text" name="pseudo" required /></label>
        <br />
        <label>mot-de-passe :<input type="password" name="mdp" required /></label>
        <br />
        <label>vérification :<input type="password" name="mdp_verif" required /></label>
        <br />
        <label>Image profil :<input type="file" name="image" accept="image/png" /></label>
        <br />
        <button type ="submit">Valider</button>

      </form>
    </fieldset>

    <?php
      }
      include('footer.php');
    ?>

 </body>
</html>
